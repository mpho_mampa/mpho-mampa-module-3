// ignore_for_file: unused_import, avoid_unnecessary_containers

import "package:flutter/material.dart";
import 'package:login/login_screen.dart';
import 'package:login/signup_screen.dart';

void main() {
  runApp(const Login());
}

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(primarySwatch: Colors.deepOrange),
        home: const Scaffold(
          body: LoginScreen(),
        ),
      ),
    );
  }
}
